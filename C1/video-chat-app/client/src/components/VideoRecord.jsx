import { useReactMediaRecorder } from "react-media-recorder";

const VideoRecord = () => {
  const { status, startRecording, stopRecording, mediaBlobUrl } =
    useReactMediaRecorder({ video: true });
 
  return (
    <div>
      <p>{status}</p>
      <button onClick={startRecording}>Start Recording</button>
      <button onClick={stopRecording}>Stop Recording</button>
      <video hidden src={mediaBlobUrl} />
      <div className="row">
            <a href={mediaBlobUrl} download> Download the video</a>
        </div>

    </div>
  );
};

export default VideoRecord;