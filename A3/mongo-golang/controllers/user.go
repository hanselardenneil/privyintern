package controllers

import (
	"encoding/json"
	"fmt"
	"mongo-golang/models"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	session *mgo.Session
}

func NewUserController(s *mgo.Session) *UserController { //return from getsession function in main.go
	return &UserController{s}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound) // check if the params is hex or not
	}

	oid := bson.ObjectIdHex(id) //if it is hex, take id to the oid var
	u := models.User{}

	if err := uc.session.DB("mongo-golang").C("users").FindId(oid); err != nil { //use oid to find is there any id that matches
		w.WriteHeader(404)
		return
	}
	uj, err := json.Marshal(u) // if id found, marshalling json to the var uj
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj) //print the value
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) { //post methods dont need any param
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u) //read json from request body and decode it to the u struct

	u.Id = bson.NewObjectId() //create new id

	uc.session.DB("mongo-golang").C("users").Insert(u) //insert u tp db after new id created

	uj, err := json.Marshal(u) //send it back to user with json

	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}
	oid := bson.ObjectIdHex(id)

	if err := uc.session.DB("mongo-golang").C("users").RemoveId(oid); err != nil { //C stands for collection
		w.WriteHeader(404)
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Deleted User", oid, "\n")
}
