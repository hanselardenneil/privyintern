
describe('Test task A1', ()=>{
    beforeEach(()=>{
        cy.viewport('macbook-16');
        cy.visit('http://codedamn.com'); 
    });
    it('Test initial page', () =>{
        //test after opening first page
        cy.contains('Learn Programming').should('have.text', 'Learn Programming');
        cy.contains('Sign In').should('exist');
        cy.contains('Sign Up').should('exist');
        cy.contains('Interactively').should('exist');
    });
    it('Test Login page', ()=>{
                //sign in page
        cy.contains('Sign In').click();
        cy.url().should('include', '/login');
    });
    it("Login should display correct error", () => {
        cy.contains('Sign In').click();
        cy.get('[data-testid="username"]').type('error', { force: true });
        cy.get('[data-testid="username"]').type('error', { force: true });
        cy.get('[data-testid="password"]').type('error', { force: true });
        cy.get('[data-testid="login"]').click({ force: true });
        cy.contains("Unable to authorize.").should('exist');
      });
});
