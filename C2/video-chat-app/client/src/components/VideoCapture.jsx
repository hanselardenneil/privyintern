import html2canvas from "html2canvas"

const VideoCapture = () => {
    const capture = () =>{
            html2canvas(document.body).then(function(canvas) {
                var image = document.createElement('a')
                image.download = `screen_capture.jpg`
                image.href = canvas.toDataURL("image/jpg").replace("image/jpg", "image/octet-system")
                image.click();
            })

}

    return ( <button onClick={capture}>Take Screen Capture</button>);
}
export default VideoCapture;
