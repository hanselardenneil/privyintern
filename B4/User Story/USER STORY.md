# Online Taxi App User Story

# Commuter’s User Story

As a Commuter, I want to book the taxi so that I reach my office on time

**Acceptance Criteria:**

1. Commuters should be able to book taxis through the taxi app with their login credentials.
2. Commuter should be able to select the taxi which reaches his pickup location in less than 5 minutes
3. Commuter should be able to select the type of vehicle he desires within the list of vehicles displayed in the app.

# Driver’s User Story

As a Taxi Driver, I want to cancel the taxi booking so that I don’t travel too far to pick up the commuter

**Acceptance Criteria:**

1. Taxi Driver should be able to see the pick-up point of the commuter to whom he has been assigned.
2. Taxi drivers should be able to cancel the trip if the commuter’s pick point is beyond 5 km.
3. Taxi drivers’ reasons of long distance pick up should be listed in ‘reason for canceling’
4. The taxi drivers should be able to see the message on canceling.
5. No charge should be levied on to Taxi driver as well customer.
6. The taxi driver should be able to receive the next booking,